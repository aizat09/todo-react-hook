import React, {useState, useEffect} from 'react'


export default function App() {

  const [todos, setTodos] = useState([])
  const [todoTitle, setTodoTitle] = useState('')

  useEffect(() => {
    const raw = localStorage.getItem('todos') || []
    setTodos(JSON.parse(raw))
  }, [])

  useEffect(() => {

    localStorage.setItem('todos', JSON.stringify(todos))
  }, [todos])

  const addTodo = event => {
    if (event.key === 'Enter' ) {
      setTodos([
        ...todos,
        {
          id: Date.now(),
          title: todoTitle,
          completed: false
        }
      ])
      console.log(todoTitle)
      setTodoTitle('')
    }
  }
  const clicked = (value, todo) => {
    const newTodos = todos.filter((item) => item.id !== todo.id)

    todo.completed = value

    setTodos([...newTodos, todo])
  }
  return (
    <div className="App">
      <header className="App-header">
        <h1>Todo app</h1>
        <div className="input-field">
          <input type="text" value={todoTitle}
                 onChange={event => setTodoTitle(event.target.value)}
                 onKeyPress={addTodo}/>
        </div>
      </header>
      <div className='container'>
        <div className="todo">
          <ul>
            {todos.map(todo =>
              <li key={todo.id} className={todo.completed ? 'completed' : ''}>
                <input type="checkbox" onChange={event => clicked(event.target.checked, todo)}/>
                <span>{todo.title} </span>
              </li>
            )}
          </ul>
        </div>
      </div>
    </div>
  );
}

